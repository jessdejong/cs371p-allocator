# CS371p: Object-Oriented Programming Allocator Repo

* Name: Jesse DeJong

* EID: jrd4535

* GitLab ID: jessdejong

* HackerRank ID: jessdejong

* Git SHA: ce783ac719478bfcd2eaf6c6468f0f47fe9398ec 

* GitLab Pipelines: https://gitlab.com/jessdejong/cs371p-allocator/-/pipelines/275136247 

* Estimated completion time: 4

* Actual completion time: 6

* Comments: Fun project, lots of debugging. Wasn't too difficult to pass on hackerrank.

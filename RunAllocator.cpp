// ----------------
// RunAllocator.c++
// ----------------

// -------- // includes
// --------

#include <iostream> // cin, cout
#include <string>   // string
#include <vector>   // vector

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */


    int TC;
    cin >> TC;
    assert(TC > 0 && TC <= 100);

    // get blank line
    string val;
    getline(cin, val);
    getline(cin, val);

    for (int i = 0; i < TC; ++i) {
        vector<int> inputs;
        getline(cin, val);

        // get allocation and deallocation requests
        while (val.compare("") != 0) {
            inputs.push_back(stoi(val));
            getline(cin, val);
        }

        assert(inputs.size() <= 1000);

        vector<double*> allocated_pointers;
        vector<int> allocated_sizes;

        // process allocation and deallocation requests
        my_allocator<double, 1000> my_heap;
        for (unsigned int j = 0; j < inputs.size(); ++j) {
            int val = inputs[j];
            if (val > 0) {
                // allocate, value is positive
                double* p = my_heap.allocate(val);
                bool added = false;
                auto x = allocated_pointers.begin();
                auto y = allocated_sizes.begin();
                for (unsigned int k = 0; k < allocated_pointers.size(); ++k) {
                    if (p < allocated_pointers[k]) {
                        allocated_pointers.insert(x, p);
                        allocated_sizes.insert(y, val);
                        added = true;
                        break;
                    }
                    ++x;
                    ++y;
                }
                if (!added) {
                    allocated_pointers.push_back(p);
                    allocated_sizes.push_back(val);
                }
            }
            else {
                // deallocate, value is negative
                my_heap.deallocate(allocated_pointers[val * -1 - 1], allocated_sizes[val * -1 - 1]);
                allocated_pointers.erase(allocated_pointers.begin() + val * -1 - 1);
                allocated_sizes.erase(allocated_sizes.begin() + val * -1 - 1);
            }
        }

        // iterate through blocks, output result
        my_allocator<double, 1000>::iterator it = my_heap.begin();
        string result = "";
        for (; it != my_heap.end(); ++it) {
            result += to_string(*it);
            result += " ";
        }
        result.pop_back();
        cout << result << endl;
    }
    return 0;
}

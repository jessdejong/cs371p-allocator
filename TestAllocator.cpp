// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read-only
    ASSERT_EQ(x[0], 992);
}                                         // fix test

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read/write
    ASSERT_EQ(x[0], 992);
}                                         // fix test

// iterator
TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(5);
    pointer p2 = allocator.allocate(3);
    pointer p3 = allocator.allocate(3);
    allocator.deallocate(p1, 5);
    allocator.deallocate(p2, 3);

    int correct[] = {72, -24, 880};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// iterator
TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(5);
    pointer p2 = allocator.allocate(3);
    allocator.deallocate(p1, 5);

    int correct[] = {40, -24, 912};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// const iterator
TEST(AllocatorFixture, test6) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type allocator;
    // pointer p1 = allocator.allocate(5);
    // pointer p2 = allocator.allocate(3);
    // allocator.deallocate(p1, 5);

    int correct[] = {992};

    allocator_type::const_iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}


// my_allocator constructor
TEST(AllocatorFixture, test7) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;

    int correct[] = {992};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// allocate method
TEST(AllocatorFixture, test8) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(3);
    pointer p2 = allocator.allocate(4);
    pointer p3 = allocator.allocate(5);
    allocator.deallocate(p3, 5);
    pointer p4 = allocator.allocate(6);
    pointer p5 = allocator.allocate(2);
    allocator.deallocate(p1, 3);

    int correct[] = {24, -32, -48, -16, 840};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// allocate method
TEST(AllocatorFixture, test9) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(23);
    pointer p2 = allocator.allocate(54);
    allocator.deallocate(p2, 54);
    pointer p3 = allocator.allocate(25);
    pointer p4 = allocator.allocate(25);
    allocator.deallocate(p3, 25);
    pointer p5 = allocator.allocate(4);
    pointer p6 = allocator.allocate(6);
    pointer p7 = allocator.allocate(3);
    allocator.deallocate(p1, 23);

    int correct[] = {184, -32, -48, -24, 72, -200, 384};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// allocate method
TEST(AllocatorFixture, test10) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(25);
    pointer p2 = allocator.allocate(25);
    pointer p3 = allocator.allocate(25);
    allocator.deallocate(p1, 25);
    allocator.deallocate(p3, 25);
    allocator.deallocate(p2, 25);

    int correct[] = {992};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// allocate method
TEST(AllocatorFixture, test11) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;

    pointer array_of_pointers[50];
    for (int i = 0; i < 50; ++i) {
        array_of_pointers[i] = allocator.allocate(1);
    }

    for (int i = 0; i < 50; ++i) {
        allocator.deallocate(array_of_pointers[i], 1);
    }

    int correct[] = {992};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// deallocate method
TEST(AllocatorFixture, test12) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(124);
    allocator.deallocate(p1, 124);

    int correct[] = {992};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// deallocate method, merge right
TEST(AllocatorFixture, test13) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(5);
    allocator.deallocate(p1, 5);

    int correct[] = {992};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// deallocate method, merge left
TEST(AllocatorFixture, test14) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(5);
    pointer p2 = allocator.allocate(5);
    allocator.deallocate(p1, 5);
    allocator.deallocate(p2, 5);

    int correct[] = {992};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// allocate longs
TEST(AllocatorFixture, test15) {
    using allocator_type = my_allocator<long, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(5);
    pointer p2 = allocator.allocate(5);
    allocator.deallocate(p1, 5);
    allocator.deallocate(p2, 5);

    int correct[] = {992};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// allocate structs
TEST(AllocatorFixture, test16) {
    struct s {
        int a;
        int b;
        int c;
    };

    using allocator_type = my_allocator<s, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;


    allocator_type allocator;
    pointer p1 = allocator.allocate(5);
    pointer p2 = allocator.allocate(5);
    allocator.deallocate(p1, 5);
    allocator.deallocate(p2, 5);

    int correct[] = {992};

    allocator_type::iterator it = allocator.begin();
    int index = 0;
    for (; it != allocator.end(); ++it) {
        ASSERT_EQ(*it, correct[index++]);
    }
}

// need to allocate full block
TEST(AllocatorFixture, test17) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(123);
    ASSERT_EQ(allocator[0], -992);
}

// check that sentinels at beginning and end are set
TEST(AllocatorFixture, test18) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(5);
    pointer p2 = allocator.allocate(5);
    pointer p3 = allocator.allocate(5);
    allocator.deallocate(p2, 5);
    ASSERT_EQ(allocator[0], -40);
    ASSERT_EQ(allocator[44], -40);
    ASSERT_EQ(allocator[48], 40);
    ASSERT_EQ(allocator[92], 40);
    ASSERT_EQ(allocator[96], -40);
    ASSERT_EQ(allocator[140], -40);
}

// check that sentinels at beginning and end are set
TEST(AllocatorFixture, test19) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type allocator;
    pointer p1 = allocator.allocate(5);
    pointer p2 = allocator.allocate(5);
    pointer p3 = allocator.allocate(5);
    allocator.deallocate(p2, 5);
    allocator.deallocate(p3, 5);
    allocator.deallocate(p1, 5);
    ASSERT_EQ(allocator[0], 992);
    ASSERT_EQ(allocator[996], 992);
}

// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& a, const iterator& b) {
            return a._p == b._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            int block_size = *_p;
            if (block_size < 0) {
                block_size *= -1;
            }
            _p += 2 + block_size / sizeof(int);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            int block_size = *(_p - 1);
            if (block_size < 0) {
                block_size *= -1;
            }
            _p -= (2 + block_size / sizeof(int));
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& a, const const_iterator& b) {
            return a._p == b._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            int block_size = *_p;
            if (block_size < 0) {
                block_size *= -1;
            }
            _p += 2 + block_size / sizeof(int);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            int block_size = *(_p - 1);
            if (block_size < 0) {
                block_size *= -1;
            }
            _p -= (2 + block_size / sizeof(int));
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * valid method returns a bool that indicates the validity of
     * the current data array by checking block traversal and sentinel
     * validity.
     */
    bool valid () const {
        // iterate through blocks
        const_iterator it = begin();
        for (; it != end(); ++it) {
            int block_size = *it;
            bool is_free = block_size > 0;
            if (block_size < 0) {
                block_size *= -1;
            }
            const int* block_start = &(*it);
            // do sentinels match?
            if (*it != *(block_start + block_size / sizeof(int) + 1)) {
                return false;
            }
            // no adjacent free blocks
            if (it != begin()) {
                int previous_block_size = *(block_start - 1);
                if (previous_block_size > 0 && is_free) {
                    return false;
                }
            }
        }

        // able to iterate through all blocks without erroring
        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // preconditions
        if (N < sizeof(T) + (2 * sizeof(int))) {
            throw std::bad_alloc();
        }

        // set original sentinels
        ((int*) a)[0] = N - 8;
        ((int*) a)[N / sizeof(int) - 1] = N - 8;

        // postconditions
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     * allocateds a free block to an allocated block, by finding the
     * first free block that fits.
     */
    pointer allocate (size_type n) {
        assert(n > 0);

        // iterate through the blocks
        long unsigned int index = 0;
        while (index < N) {
            int block_size = (int) (*this)[index];
            if (block_size < 0) {
                // this block is already allocated
                block_size *= -1;
                index += 2 * sizeof(int) + block_size;
            }
            else {
                // block hasn't been allocated
                int num_bytes_needed = 2 * sizeof(int) + n * sizeof(T);
                int num_bytes_available = 2 * sizeof(int) + block_size;

                if (num_bytes_available - num_bytes_needed < int(sizeof(T) + (2 * sizeof(int))) && num_bytes_available - num_bytes_needed >= 0) {
                    // allocate this entire free block
                    int num_bytes = num_bytes_available - 2 * sizeof(int);
                    ((int*) a)[index / sizeof(int)] = -1 * num_bytes;
                    ((int*) a)[(index + sizeof(int) + num_bytes) / sizeof(int)] = -1 * num_bytes;

                    assert(valid());
                    return (pointer) &(a[index + sizeof(int)]);
                }
                else if (num_bytes_available - num_bytes_needed >= int(sizeof(T) + (2 * sizeof(int)))) {
                    // allocate this block, but create a free block at the end
                    ((int*) a)[index / sizeof(int)] = int(n * sizeof(T)) * -1;
                    ((int*) a)[(index + sizeof(int) + n * sizeof(T)) / sizeof(int)] = int(n * sizeof(T)) * -1;

                    int next_block_index = (index + 2 * sizeof(int) + n * sizeof(T));

                    ((int*) a)[next_block_index / sizeof(int)] = (num_bytes_available - num_bytes_needed - 2 * sizeof(int));
                    ((int*) a)[(next_block_index + sizeof(int) + (num_bytes_available - num_bytes_needed - 2 * sizeof(int))) / sizeof(int)] = (num_bytes_available - num_bytes_needed - 2 * sizeof(int));

                    assert(valid());
                    return (pointer) &(a[index + sizeof(int)]);
                }
                else {
                    // not enough room in this block to allocate
                    index += 2 * sizeof(int) + block_size;
                }
            }
        }

        // no block fits
        throw std::bad_alloc();
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * marks the block at p as free, and merges the free block with
     * free blocks on the right and left.
     */
    void deallocate (pointer p, size_type n) {
        // change the sign of the sentinels at block beginning with p
        int* new_p = (int*) p;
        new_p--;
        int* start_of_original_block = new_p;
        int this_block_size = *start_of_original_block * -1;
        *new_p = this_block_size;
        new_p += (sizeof(int) + this_block_size) / sizeof(int);
        *new_p = this_block_size;

        // merge with next block, if next block is free
        if (new_p != &(((int*) a)[N / sizeof(int) - 1])) {
            ++new_p;
            int next_block_size = *new_p;
            if (next_block_size > 0) {
                *start_of_original_block = this_block_size + next_block_size + 2 * sizeof(int);
                start_of_original_block[(sizeof(int) + this_block_size + next_block_size + 2 * sizeof(int)) / sizeof(int)] = this_block_size + next_block_size + 2 * sizeof(int);
                this_block_size = this_block_size + next_block_size + 2 * sizeof(int);
            }
        }

        // merge with previous block, if previous block is free
        // new_p is at the beginning of the original block that was freed
        new_p = start_of_original_block;
        if (new_p != &(((int*) a)[0])) {
            new_p--;
            int previous_block_size = *new_p;
            new_p++;
            new_p -= (2 * sizeof(int) + previous_block_size) / sizeof(int);
            if (previous_block_size > 0) {
                *new_p = (previous_block_size + this_block_size + 2 * sizeof(int));
                new_p += (sizeof(int) + previous_block_size + this_block_size + 2 * sizeof(int)) / sizeof(int);
                *new_p = (previous_block_size + this_block_size + 2 * sizeof(int));
            }
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
